"use strict";
jQuery(document).ready(function($){
  // Window on load
  $('.banner__slider .banner__slider--content .owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    autoplay:true,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:1
      },
      1000:{
        items:1
      }
    }
  });
  $('.header__new--content .owl-carousel').owlCarousel({
    loop: true,
    autoplay: true,
    items: 1,
    nav: false,
    dots:false,
    animateOut: 'slideOutUp',
    animateIn: 'slideInUp'
  });
  $(window).on('load', function(){

  });
  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:false,
    autoplay:true,
    responsive:{
      0:{
          items:1
      },
      600:{
          items:3
      },
      1000:{
          items:5
      },
      1366:{
        items:4
      }
    }
  });
  $('.btn-menu-mobile').click(function(e){
    if (!e)
      e = window.event;
    //IE9 & Other Browsers
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    //IE8 and Lower
    else {
      e.cancelBubble = true;
    }
    $('.menu-mobile').addClass('active');
    $('#wrapper').addClass('active');
  });
  $('.exit').click(function(){
    $('.menu-mobile').removeClass('active');
    $('#wrapper').removeClass('active');
  });
  $('.bg').click(function() {
    $('.menu-mobile').removeClass('active');
    $('#wrapper').removeClass('active');
});
});
$(window).on('resize', function(){
  var win = $(this); //this = window
  if (win.width() >= 992) {
    $('.menu-mobile').removeClass('active');
    $('#wrapper').removeClass('active');
  }
});